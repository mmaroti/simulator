/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_PSD_LOGGER_H
#define INCLUDED_RIRCSIM_PSD_LOGGER_H

#include <rircsim/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace rircsim {

    /*!
     * \brief <+description of block+>
     * \ingroup rircsim
     *
     */
    class RIRCSIM_API psd_logger : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<psd_logger> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of rircsim::psd_logger.
       *
       * To avoid accidental use of raw pointers, rircsim::psd_logger's
       * constructor is in a private implementation
       * class. rircsim::psd_logger::make is the public interface for
       * creating new instances.
       */
      static sptr make(std::string node);
    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_PSD_LOGGER_H */

