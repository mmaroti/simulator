/* -*- c++ -*- */

#define RIRCSIM_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "rircsim_swig_doc.i"

%{
#include "rircsim/muxer.h"
#include "rircsim/liquid_flex_rx.h"
#include "rircsim/liquid_flex_tx.h"
#include "rircsim/psd_logger.h"
%}

%include "rircsim/muxer.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, muxer);

%include "rircsim/liquid_flex_rx.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, liquid_flex_rx);

%include "rircsim/liquid_flex_tx.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, liquid_flex_tx);
%include "rircsim/psd_logger.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, psd_logger);
