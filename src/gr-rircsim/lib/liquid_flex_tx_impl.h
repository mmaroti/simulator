/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_LIQUID_FLEX_TX_IMPL_H
#define INCLUDED_RIRCSIM_LIQUID_FLEX_TX_IMPL_H

#include <rircsim/liquid_flex_tx.h>
#include <vector>
#include <queue>
#include <liquid/liquid.h>

namespace gr {
  namespace rircsim {

    class liquid_flex_tx_impl : public liquid_flex_tx
    {
     private:
      flexframegen m_framegen;
      flexframegenprops_s m_fgprops;
      std::vector<gr_complex> m_iq_buffer;
      std::queue<pmt::pmt_t> m_pdu_queue;
      void in_pdu_handler(pmt::pmt_t msg);

     public:
      liquid_flex_tx_impl(int mod, int fec1, int fec2);
      ~liquid_flex_tx_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_LIQUID_FLEX_TX_IMPL_H */

