/* -*- c++ -*- */
/*
 * Copyright 2020 RIRC.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "liquid_flex_rx_impl.h"

namespace gr {
  namespace rircsim {

    liquid_flex_rx::sptr
    liquid_flex_rx::make()
    {
      return gnuradio::get_initial_sptr
        (new liquid_flex_rx_impl());
    }


    /*
     * The private constructor
     */
    liquid_flex_rx_impl::liquid_flex_rx_impl()
      : gr::block("liquid_flex_rx",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(0, 0, 0))
    {
      m_framesync = flexframesync_create(frame_callback, (void *)this);
      // message_port_register_in(pmt::mp("in_iq"));
      // set_msg_handler(pmt::mp("in_iq"), [this](pmt::pmt_t msg) { this->in_iq_handler(msg); });     
      
      message_port_register_out(pmt::mp("out_pdu"));
    }

    int liquid_flex_rx_impl::frame_callback(unsigned char *  _header,
                                            int              _header_valid,
                                            unsigned char *  _payload,
                                            unsigned int     _payload_len,
                                            int              _payload_valid,
                                            framesyncstats_s _stats,
                                            void *           _userdata)
    {
      if(_header_valid && _payload_valid)
      {
        pmt::pmt_t vector_pmt(pmt::make_blob(_payload, _payload_len));
        pmt::pmt_t pdu(pmt::cons(pmt::PMT_NIL, vector_pmt));
        liquid_flex_rx_impl * l = (liquid_flex_rx_impl *)_userdata;
        l->message_port_pub(pmt::intern("out_pdu"), pdu);
      }      
      return 0;
    }

    /*
     * Our virtual destructor.
     */
    liquid_flex_rx_impl::~liquid_flex_rx_impl()
    {
    }


    int liquid_flex_rx_impl::general_work(int noutput_items,
                                          gr_vector_int &ninput_items,
                                          gr_vector_const_void_star &input_items,
                                          gr_vector_void_star &output_items)
    {
      flexframesync_execute(m_framesync, (gr_complex *)input_items[0], noutput_items);
      consume(0, noutput_items);
      return 0;
    }

  } /* namespace rircsim */
} /* namespace gr */

