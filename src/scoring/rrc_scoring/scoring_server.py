import secrets
import zmq
import multiprocessing
import threading
import time
import logging
import jwt
import hashlib
import pmt

log = logging.getLogger('ScoringServer')


class ScoringServer():
  def __init__(self, pub_addr="127.0.0.1", pub_port=44001, sub_addr="127.0.0.1", sub_port=44002, traffic_config=None):
    self.pub_addr = "tcp://" + pub_addr + ":" + str(pub_port)
    self.sub_addr = "tcp://" + sub_addr + ":" + str(sub_port)

    print(self.pub_addr)
    print(self.sub_addr)

    self.pub_proc = multiprocessing.Process(target=self.pub_fn, args=[])
    self.sub_proc = multiprocessing.Process(target=self.sub_fn, args=[])
    self.pub_proc.daemon = True
    self.sub_proc.daemon = False

    self.traffic_config = traffic_config
    self.bitrate = traffic_config['bitrate']
    self.packet_size = traffic_config['packet_size']
    self.signing_secret = traffic_config['hmac_secret']
    #self.signing_secret = traffic_config['hmac_secret'].encode('utf-8')

    self.packet_period = 8.0*self.packet_size / self.bitrate

    self.seq_tx = 1
    self.score_lock = threading.Lock()
    self.score = 0

  # convert a byte string to a serialized PMT blob
  def bytes_to_serialized_pmt(self, pld):
    v = pmt.init_u8vector(len(pld), pld)
    d = pmt.cons(pmt.to_pmt(len(pld)), v)
    s = pmt.serialize_str(d)
    return s

  # convert a serialized PMT blob to byte string
  def serialized_pmt_to_bytes(self, pld):
    p = pmt.deserialize_str(pld)
    b = pmt.cdr(p)
    d = pmt.to_python(b)
    s = d.tostring()
    return s

  def pub_fn(self):
    self.pub = zmq.Context().socket(zmq.PUB)
    self.pub.bind(self.pub_addr)

    time.sleep(1.0)

    while True:
      p = self.build_packet()
      p_pmt = self.bytes_to_serialized_pmt(p)
      status = self.pub.send(p_pmt)
      time.sleep(0.1)

    while True:
      time.sleep(1.0)

  def sub_fn(self):
    self.sub = zmq.Context().socket(zmq.SUB)
    self.sub.connect(self.sub_addr)
    self.sub.subscribe("")

    # zero-out score file from prior run
    with open("/scripts/scoring/score.txt", "wb") as f:
      f.write(b"%d" % 0)

    while True:
      if self.sub.poll(timeout=0):
        r = self.sub.recv()
        p = None

        r = self.serialized_pmt_to_bytes(r)

        # Try to decode the JWT
        try:
          #p = jwt.decode(r, "bad key", algorithm="HS256", verify=True)
          p = jwt.decode(r, self.signing_secret, algorithm="HS256", verify=True)
        except jwt.exceptions.InvalidSignatureError:
          log.info("Failed signature check!")

        # if the decode worked, process its sequence number
        if p is not None:
          log.info("Received seq: {}".format(p["seq"]))

          # if the sequence number is the current score + 1, it's a contiguous monotonic packet so score it
          self.score_lock.acquire()
          if p["seq"] == (self.score + 1):
            log.info("New score: {}".format(p["seq"]))

            # TODO argparse config
            with open("/scripts/scoring/score.txt", "wb") as f:
              f.write(b"%d" % p["seq"])

            self.score = p["seq"]
          # else we dropped or got a duplicate, so no more packets will score
          else:
            log.warn("MISS: Expecting {}, got {}".format(self.score+1, p["seq"]))
          self.score_lock.release()

      else:
        time.sleep(0.01)

  def build_packet(self):
    ''' packet format:
    jwt
    '''

    #r = str(secrets.token_bytes(nbytes=self.packet_size))
    r = str(secrets.token_bytes(nbytes=self.packet_size).hex())
    p = {"seq": self.seq_tx, \
         "payload": r}
    packet = jwt.encode(p, self.signing_secret, algorithm="HS256")
    log.info("Sending seq: {}, length: {}".format(self.seq_tx, len(packet)))
    self.seq_tx += 1

    return packet

  def run(self):
    self.sub_proc.start()
    self.pub_proc.start()

    log.info("Running...")
#    while True:
#      time.sleep(0.01)

  def stop(self):
    log.info("Stopping...")
    self.pub_proc.terminate()
    self.sub_proc.terminate()


def main():
  ss = ScoringServer()
  ss.run()

if __name__ == '__main__':
  main()

