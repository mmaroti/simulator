import multiprocessing
import time
import logging

from .scoring_server import ScoringServer

log = logging.getLogger('MatchRunner')

class MatchRunner():
  def __init__(self, src_ip="127.0.0.1", src_port=44001, sink_ip="127.0.0.1", sink_port=44002, config=None):
    self.src_ip = src_ip
    self.src_port = src_port
    self.sink_ip = sink_ip
    self.sink_port = sink_port
    self.pre_match = config['pre_match']
    self.duration = config['duration']
    self.post_match = config['post_match']
    log.info("Match config: duration {}, pre-match {}, post-match {}".format(self.duration, self.pre_match, self.post_match))

    self.ss = ScoringServer(self.src_ip, self.src_port, self.sink_ip, self.sink_port, config['traffic_config'])

  def run(self):
    t_init = time.time()
    log.info("Starting pre-match at {}".format(t_init))
    while t_init + self.pre_match > time.time():
      time.sleep(0.01)

    log.info("Starting ScoringServer.")
    self.ss.run()

    t_start = time.time()
    log.info("Starting match at {}".format(t_start))
    while (t_start + self.duration) > time.time():
      time.sleep(0.01)

    log.info("Match duration elapsed at {}".format(time.time()))

    t_start = time.time()
    log.info("Starting post-match at {}".format(t_start))
    while (t_start + self.post_match) > time.time():
      time.sleep(0.01)

    return None

  def stop(self):
    t_stop = time.time()
    log.info("Stopping match at {}".format(t_stop))
    score = 0
    try:
      self.ss.stop()
    except:
      pass


def main():
  score = 0
  mr = MatchRunner()
  mr.run()
  log.critical("Done")
  mr.stop()


if __name__ == '__main__':
  main()

