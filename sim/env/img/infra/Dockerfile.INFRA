FROM ubuntu:20.04 as gnuradio-base

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get update --fix-missing
RUN apt-get install -y gnuradio-dev libgtk-3-dev git cmake libboost-all-dev swig liborc-0.4-dev
RUN apt-get install -y xterm

ARG target_uid=default-uid
ENV target_uid=$target_uid
RUN echo $target_uid
RUN useradd -m -s /bin/bash -u $target_uid developer

RUN mkdir -p /home/developer/src

ADD patches/qdetector_cccf.patch /tmp/qdetector_cccf.patch
ADD patches/dotprod_crcf.av.patch /tmp/dotprod_crcf.av.patch
ADD patches/dotprod_crcf.neon.patch /tmp/dotprod_crcf.neon.patch
ADD patches/dotprod_crcf.mmx.patch /tmp/dotprod_crcf.mmx.patch

RUN cd /home/developer/src && \
    git clone https://github.com/jgaeddert/liquid-dsp.git && \
    cd liquid-dsp && \
    patch src/framing/src/qdetector_cccf.c /tmp/qdetector_cccf.patch && \
    patch src/dotprod/src/dotprod_crcf.av.c /tmp/dotprod_crcf.av.patch && \
    patch src/dotprod/src/dotprod_crcf.neon.c /tmp/dotprod_crcf.neon.patch && \
    patch src/dotprod/src/dotprod_crcf.mmx.c /tmp/dotprod_crcf.mmx.patch && \
    ./bootstrap.sh && \
    LIBS="-lvolk" ./configure --prefix=/usr --exec-prefix= && \
    make -j && \
    make DESTDIR= install && \
    ldconfig

RUN apt-get install -y libzmq3-dev
RUN apt-get install -y python3-pip
RUN pip3 install pyjwt

ADD gr-rircsim /tmp/src/gr-rircsim

RUN cp -r /tmp/src/gr-rircsim /home/developer/src/

RUN cd /home/developer/src && \
    cd gr-rircsim && \
    rm -rf build && \
    mkdir -p build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr ../ && \
    make -j && \
    make install && \
    ldconfig

RUN echo "xterm_executable = /usr/bin/xterm" >> /etc/gnuradio/conf.d/grc.conf

RUN mkdir -p /scripts

USER developer
ENV HOME /home/developer


# CMD ["gnuradio-companion", "/grc/base.grc"]